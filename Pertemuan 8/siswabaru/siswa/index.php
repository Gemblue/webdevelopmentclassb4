<?php

include('../connection.php');

$sql = "SELECT * FROM tbl_siswa
        JOIN tbl_sekolah ON tbl_sekolah.id = tbl_siswa.id_sekolah
        JOIN tbl_kota ON tbl_kota.id = tbl_siswa.id_kota
        ";
$query = mysqli_query($connect, $sql);
$results = mysqli_fetch_all($query, MYSQLI_ASSOC);
?>

<a href="add.php">Tambah Data</a>

<br/><br/>

<table border="1">
    <tr>
        <th>No</th>
        <th>Nama Siswa</th>
        <th>Tempat Lahir</th>
        <th>Tanggal Lahir</th>
        <th>Sekolah</th>
        <th>Kota</th>
        <th></th>
    </tr>
    <?php $num = 1;foreach($results as $result) : ?>    
        <tr>
            <td><?php echo $num;?></td>
            <td><?php echo $result['nama_siswa']?></td>
            <td><?php echo $result['tempat_lahir']?></td>
            <td><?php echo $result['tanggal_lahir']?></td>
            <td><?php echo $result['nama_sekolah']?></td>
            <td><?php echo $result['nama']?></td>
            <td>
                <a href="edit.php?id_siswa=<?php echo $result['id_siswa']?>">Edit</a>
                <a href="delete.php?id_siswa=<?php echo $result['id_siswa']?>">Delete</a>
            </td>
        <tr>
    <?php $num++; endforeach;?>
</table>