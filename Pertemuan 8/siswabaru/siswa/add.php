<?php
include('../connection.php');

$query = mysqli_query($connect, 'SELECT * FROM tbl_sekolah');
$schools = mysqli_fetch_all($query, MYSQLI_ASSOC);

$query = mysqli_query($connect, 'SELECT * FROM tbl_kota');
$cities = mysqli_fetch_all($query, MYSQLI_ASSOC);

?>

<form action="insert.php" method="post">
    <label for="">Nama Siswa</label><br/>
    <input type="text" name="nama_siswa" /><br/><br/>

    <label for="">Tempat Lahir</label><br/>
    <input type="text" name="tempat_lahir" /><br/><br/>
    
    <label for="">Tanggal Lahir</label><br/>
    <input type="date" name="tanggal_lahir" /><br/><br/>

    <label for="">Jurusan</label><br/>
    <select name="jurusan">
        <option value="TKJ">TKJ</option>
        <option value="IPA">IPA</option>
        <option value="IPS">IPS</option>
    </select><br/><br/>

    <label for="">Sekolah</label><br/>
    <select name="id_sekolah">
        <?php foreach($schools as $school) :?>
            <option value="<?php echo $school['id']?>"><?php echo $school['nama_sekolah']?></option>
        <?php endforeach;?>
    </select><br/><br/>

    <label for="">Kota</label><br/>
    <select name="id_kota">
        <?php foreach($cities as $city) :?>
            <option value="<?php echo $city['id']?>"><?php echo $city['nama']?></option>
        <?php endforeach;?>
    </select><br/><br/>

    <button type="submit">Save</button>
</form>