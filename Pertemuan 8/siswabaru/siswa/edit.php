<?php
include('../connection.php');

$get = $_GET;

$query = mysqli_query($connect, "SELECT * FROM tbl_siswa WHERE id_siswa = '$get[id_siswa]'");
$result = mysqli_fetch_all($query, MYSQLI_ASSOC);

$query = mysqli_query($connect, 'SELECT * FROM tbl_sekolah');
$schools = mysqli_fetch_all($query, MYSQLI_ASSOC);

$query = mysqli_query($connect, 'SELECT * FROM tbl_kota');
$cities = mysqli_fetch_all($query, MYSQLI_ASSOC);

?>

<form action="update.php" method="post">
    <input type="hidden" name="id_siswa" value="<?php echo $result[0]['id_siswa']?>" />

    <label for="">Nama Siswa</label><br/>
    <input type="text" name="nama_siswa" value="<?php echo $result[0]['nama_siswa']?>" required/><br/><br/>
    
    <label for="">Tempat Lahir</label><br/>
    <input type="text" name="tempat_lahir" value="<?php echo $result[0]['tempat_lahir']?>" required/><br/><br/>

    <label for="">Tanggal Lahir</label><br/>
    <input type="date" name="tanggal_lahir" value="<?php echo $result[0]['tanggal_lahir']?>" required/><br/><br/>

    <label for="">Jurusan</label><br/>
    <select name="jurusan">
        <option value="IPA" <?php echo ($result[0]['jurusan'] == 'IPA') ? 'selected' : ''?> >IPA</option>
        <option value="TKJ" <?php echo ($result[0]['jurusan'] == 'TKJ') ? 'selected' : ''?>>TKJ</option>
        <option value="IPS" <?php echo ($result[0]['jurusan'] == 'IPS') ? 'selected' : ''?>>IPS</option>
    </select><br/><br/>

    <label for="">Sekolah</label><br/>
    <select name="id_sekolah">
        <?php foreach($schools as $school) :?>
            <option value="<?php echo $school['id']?>" <?php echo ($school['id'] == $result[0]['id_sekolah']) ? 'selected' : ''?> ><?php echo $school['nama_sekolah']?></option>
        <?php endforeach;?>
    </select><br/><br/>

    <label for="">Kota</label><br/>
    <select name="id_kota">
        <?php foreach($cities as $city) :?>
            <option value="<?php echo $city['id']?>" <?php echo ($city['id'] == $result[0]['id_kota']) ? 'selected' : ''?>><?php echo $city['nama']?></option>
        <?php endforeach;?>
    </select><br/><br/>

    <button type="submit">Save</button>
</form>