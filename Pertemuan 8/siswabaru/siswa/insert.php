<?php

include('../connection.php');

$post = $_POST; // Ngambil data dari POST.

// Validasi
if (empty($post['nama_siswa']))
{
    echo 'Nama siswa harus diisi ..';
    exit;
}

if (empty($post['tempat_lahir']))
{
    echo 'Templat lahir harus diisi ..';
    exit;
}

$tanggal_lahir = date('Y-m-d', strtotime($post['tanggal_lahir']));

// Pengecekan double siswa.
$query = mysqli_query($connect, "SELECT * FROM tbl_siswa WHERE nama_siswa = '$post[nama_siswa]' AND tanggal_lahir = '$post[tanggal_lahir]'");
$siswa = mysqli_fetch_all($query, MYSQLI_ASSOC);

if (!empty($siswa))
{
    echo 'Siswa sudah ada ..';
    exit;
}

$sql = "INSERT INTO tbl_siswa SET 
        nama_siswa='$post[nama_siswa]', 
        tempat_lahir='$post[tempat_lahir]', 
        tanggal_lahir='$tanggal_lahir', 
        jurusan='$post[jurusan]',
        id_sekolah='$post[id_sekolah]',
        id_kota='$post[id_kota]'";

$insert = mysqli_query($connect, $sql);

if ($insert)
{
    header('Location: index.php');
}
else
{
    echo 'Gagal ..';
}