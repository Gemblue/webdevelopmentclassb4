<?php
    include("../connect.php");

    $title = null;
    $content = null;

    if (isset($_GET['title'])) {
        $title = $_GET['title'];
    }

    if (isset($_GET['content'])) {
        $content = $_GET['content'];
    }

    $query = mysqli_query($connect, "SELECT * FROM article 
    JOIN user ON user.id_user = article.id_user 
    JOIN category ON category.id_category = article.id_category
    WHERE title LIKE '%$title%' 
    AND content LIKE '%$content%'
    ");

    $results = mysqli_fetch_all($query, MYSQLI_ASSOC);
    
?>

<a href="add.php"> Tambah Data </a>
<br/><br/>

<form action="index.php" method="get">
    <input type="text" name="title" value="<?php echo $_GET['title']?>" placeholder="title"/>
    <input type="text" name="content" value="<?php echo $_GET['content']?>" placeholder="content"/>
    <button type="submit">Cari</button>
</form>

<br/>

<?php if (!empty($results)) :?>

    <table border=1>
        <tr>
            <th> No </th>
            <th> Tanggal </th>
            <th> Judul </th>
            <th> Penulis </th>
            <th> Kategori </th>
            <th> </th>
        </tr>
        <?php $no=1; foreach($results as $result) : ?>
        <tr>
            
            <td> <?php echo $no ?> </td>
            <td> <?php echo $result['date_pub'] ?> </td>
            <td> <?php echo $result['title'] ?> </td>
            <td> <?php echo $result['author_name'] ?> </td>
            <td> <?php echo $result['category'] ?> </td>
            <td> 
                <a href='edit.php?id=<?php echo $result['id'] ?>'>Edit</a>
                <a href='delete.php?id=<?php echo $result['id'] ?>'>Delete</a> 
            </td>
        
        </tr>
        <?php $no++; endforeach; ?>
    </table>

<?php else:?>
    Data tidak ditemukan ..
<?php endif;?>