<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
        crossorigin="anonymous">
    <link rel="stylesheet" href="style/css/main.css">

    <title>Info Seleb</title>

</head>

<body>
    <!--First Container   -->
    <div class="container" style="margin:0px"></div>
    <?php 
        // include ("menu.php");
        include ("mainMenu.php");
        include ("header.php");
        include ("content.php");
        include ("headerBottom.php");
        include ("footer.php");
    ?>
    
    