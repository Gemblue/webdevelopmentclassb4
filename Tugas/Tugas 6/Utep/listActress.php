<h3>Top 100 Actresses</h3>
<hr>
<table class="table table-triped">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th scope="col" width="150px">Photo</th>
            <th scope="col">Name</th>
            <th scope="col">Biography</th>
            
        </tr>
    </thead>
    <tbody>
        <?php
            $no=1;
            $list = file_get_contents('listActress.json');
            $actress = json_decode($list, true);
            
            foreach($actress as $k)
            {
        ?>
        <tr>
            <th scope="row">
                <?php echo $no;?>
            </th>
            <td>
                <img src="<?php echo $k["img"];?>" alt="" class="img-thumbnail" style="width:120px;height:150px">
            </td>
            <td>
                <strong>
                    <?php echo $k["name"];?>
                </strong>
            </td>
            <td style="text-align:justify">
                <?php echo $k["history"];?>
            </td>
            
        </tr>
        <?php $no++; }?>
    </tbody>
</table>