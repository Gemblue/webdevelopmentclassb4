<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
        crossorigin="anonymous">
    <link rel="stylesheet" href="style/css/main.css">

    <title>Info Seleb</title>

</head>

<body>
    <!--First Container   -->
    <div class="container" style="margin:0px"></div>
    <?php include ("mainMenu.php"); ?>
    <?php include ("header.php"); ?>
    
    <!-- First Container Fluid-->
    <div class="container-fluid">
    <div class="row">
        <div class="col-sm-2" style="">
            Panel kiri
        </div>
        <div class="col-sm-8" style="">
            About US
        </div>
        <div class="col-sm-2" style="">
            Panel Kanan
        </div>
    </div>

    <?php include ("headerBottom.php"); ?>
    <?php include ("footer.php");?>
    
    