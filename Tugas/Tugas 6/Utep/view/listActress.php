<table class="table">
    <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">History</th>
            <th scope="col">Photo</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $no=1;
            $list = file_get_contents('listActress.json');
            $actress = json_decode($list, true);
            
            foreach($actress as $k)
            {
        ?>
        <tr>
            <td scope="row">
                <?php echo $no;?>
            </td>
            <td scope="row">
                <?php echo $k["name"];?>
            </td>
            <td scope="row">
                <?php echo $k["history"];?>
            </td>
            <td scope="row">
                <?php echo $k["img"];?>
            </td>
        </tr>
        <?php $no++ ; }?>
    </tbody>
</table>