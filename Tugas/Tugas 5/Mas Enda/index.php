<!DOCTYPE <!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>List Artis Indonesia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--ini bootstrap-->
    <link rel="stylesheet" type="text/css" media="screen" href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" /> <!--Donwload bootstrap melalui alamat-->
    <!--ini custom-->
    <link rel="stylesheet" href="main.css">
    <!--alamat jquery-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--js bootstrap-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <style>
      .jumbotron{
        margin: 0;
        height: 300px;
        padding: 10px;
        background:white;
      }
      .footer{
          margin-top:50px;
          text-align:center;
      }
    </style>

  </head>
<body>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Brand</a>
      </div>
  
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
          <li><a href="#">Gallery</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kategori <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#">Artis Komedi</a></li>
              <li><a href="#">Artis Sinetron</a></li>
              <li><a href="#">Presenter</a></li>
              <li><a href="#">Penyanyi</a></li>

            </ul>
          </li>
        </ul>
        <form class="navbar-form navbar-left">
          <div class="form-group">
            <input type="text" class="form-control" placeholder="Pencarian nama artis">
          </div>
          <button type="submit" class="btn btn-default">Cari</button>
        </form>

      </div><!-- /.navbar-collapse -->     
    </div><!-- /.container-fluid -->
    </nav>  
    <div class="jumbotron">
        <h3>Artis Film Indonesia</h3>    
        <?php
        $karyawan = array(
            array(
                'foto'=> 'gambar/stevan.jpg',
                'nama' => 'Stefan William',
                'namalain' => 'Stefan William Umboh',
                'tgllahir' => '11 Agustus 1993',
                'profesi' => 'Aktor, penyanyi dan model'
            ),
            array(
                'foto'=> 'gambar/afgan.jpg',
                'nama' => 'Afgansyah Reza',
                'namalain' => 'Afgan/Agan/Atenk',
                'tgllahir' => 'Jakarta, 27 Mei 1989',
                'profesi' => 'Penyanyi'
            ),
            array(
                'foto'=> 'gambar/ahmaddani.jpg',
                'nama' => 'Ahmad Dhani Prasetyo',
                'namalain' => 'Dhani Manaf',
                'tgllahir' => '20-02-1988',
                'profesi' => 'Penyanyi, Pencipta lagu'
            ),
            array(
                'foto'=> 'gambar/adly.jpg',
                'nama' => ' Raffi Faridz Ahmad',
                'namalain' => ' Raffi Ahmad',
                'tgllahir' => 'Bandung, Jawa Barat',
                'profesi' => 'Aktor, Penyanyi, Presenter'
            ),
            array(
                'foto'=> 'gambar/adly.jpg',
                'nama' => 'Ahmad Adly Fairuz',
                'namalain' => 'Ahmad Fairuz',
                'tgllahir' => '20-02-1988',
                'profesi' => 'Aktor, Penyanyi'
            ),
            array(
                'foto'=> 'gambar/adly.jpg',
                'nama' => 'Muhammad Aliando Syarief',
                'namalain' => 'Aliando',
                'tgllahir' => '20-02-1988',
                'profesi' => 'Aktor'
            )

            );
            echo "<table class='table table-hover'>";
            echo "<tr>
                    <th>foto</th>
                    <th>Nama</th>
                    <th>Nama Lain</th>
                    <th>Tanggal Lahir</th>
                    <th>Profesi</th>
                
            </tr>";

        foreach ($karyawan as $k)
        {
            echo '<tr><td><img width="100" src="' . $k[foto] . '"/></td><td>' . $k['nama'] . '</td><td>' . $k['namalain'] . '</td><td>' . $k['tgllahir'] . '</td><td>' . $k['profesi'] . '</td></tr>';
        }
        echo "</table>"

        ?>
    <div class="footer">
    <marquee width="20%" style=""><b>Informasi Artis Indonesia</b></marquee>
    </div>
    </div>

</body>
</html>