<?php

// Pemilihan If Else

// 1-3  D
// 4-6  C
// 7-8  B
// 9-10 A

// $nilai = 8;

// if ($nilai >= 1 && $nilai <=3) 
// {
//     echo "D";
// } 
// else 
// {
//     echo "B";
// }

// If bersarang.
// $diskon = true;
// $harga = 70000;

// if ($diskon == true) 
// {
//     if ($harga < 50000)
//     {
//         echo 'Saya belanja!';
//     }
//     else
//     {
//         echo 'Gak belanja karena mahal.';
//     }
// }
// else
// {
//     echo 'Gak belanja karena gak ada diskon.';
// }

// Nested If

// $umur = 18;
// $calon = true;
// $biaya = 6000;

// if ($umur > 17)
// {
//     if ($calon == true)
//     {
//         if ($biaya > 5000)
//         {
//             echo 'Menikah Yeay!';
//         }
//         else
//         {
//             echo 'Biaya gak cukup';
//         }
//     }
//     else
//     {
//         echo 'Gak ada calon';
//     }
// }
// else
// {
//     echo 'Umur belum cukup';
// }

// Switch
$haywan = "kalb";

switch($haywan) 
{
	case "hirroh":
    	echo "Kucing";
    break;
    
    case "kalb":
    	echo "Anjing";
    break;
    
    case "qird":
        echo "Monyet";
    break;
    
    default:
    	echo "Manusia";
}