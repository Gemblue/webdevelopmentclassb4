<?php

$karyawan = ['Oriza', 'Enda', 'Yusuf'];

// Memanggil salah satu index
echo $karyawan[0];

echo '<hr/>';

// Memanggil satu satu berulang tanpa index
foreach($karyawan as $k)
{
    echo $k . '<br/>';
}

echo '<hr/>';

// Memanggil satu satu berulang dengan index
foreach($karyawan as $key => $value)
{
    echo $key . ' ' . $value . '<br/>';
}

echo '<hr/>';

$karyawan = [
    [
        'nama' => 'Oriza',
        'umur' => 20
    ],
    [
        'nama' => 'Utep',
        'umur' => 17
    ],
    [
        'nama' => 'Topo',
        'umur' => 18
    ]
];

echo '<table border="1">';

echo '<tr><th>Nama</th><th>Umur</th></tr>';

foreach ($karyawan as $k)
{
    echo '<tr><td>'. $k['nama'] .'</td><td>'. $k['umur'] .'</td></tr>';
}

echo '</table>';