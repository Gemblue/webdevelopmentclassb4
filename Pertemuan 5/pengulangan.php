<?php
// For

// for($i=1; $i<=5; $i++)
// {
//     echo $i . '<br/>';
// }

// While
// $i = 0;

// while ($i <= 5)
// {
//     echo $i . '<br/>';

//     $i++;
// }

// Do While
$i = 10;

do 
{
    echo $i . '<br/>';
    $i++;
}
while ($i <= 5);