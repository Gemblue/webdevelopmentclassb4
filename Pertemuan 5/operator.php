<?php

// Operator Aritmatika

$panjang = 100;
$lebar = 20;

$luas = $panjang * $lebar;

echo $luas;

define('PI', 3.14);
$r = 20;

$luas = PI * ($r * $r);

echo $luas;

$a = 20;
$b = 50;

echo $a + $b . '<br/>';
echo $a - $b . '<br/>';
echo $a / $b . '<br/>';
echo $a * $b . '<br/>';
echo $a ** $b . '<br/>';

echo '<hr/>';

// Operator Increment/Decrement
$a = 1;
$a++;
echo $a;

$b = 5;
$b--;
echo $b;

echo '<hr/>';

// Operator Perbandingan
$a = 5;
$b = 10;

echo $a == $b;
echo $a != $b;
echo $a === $b;
echo $a > $b;

echo '<hr/>';

// Operator Logika
$a = true;
$b = false;

echo $a and $b;
echo $a or $b;

echo '<hr/>';

// Operator Konkatenansi.
$nama = "Toni Haryanto";
echo "Nama saya " . $nama;