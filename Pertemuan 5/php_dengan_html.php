<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>

<?php
$karyawan = [
    [
        'nama' => 'Oriza',
        'umur' => 20,
        'nilai' => 50
    ],
    [
        'nama' => 'Utep',
        'umur' => 17,
        'nilai' => 70
    ],
    [
        'nama' => 'Topo',
        'umur' => 18,
        'nilai' => 80
    ]
];
?>

<table border="1">
    <tr><th>Nama</th><th>Umur</th><th>Nilai</th><th>Grade</th></tr>
    <?php foreach($karyawan as $k) :?>
        <tr>
            <td><?php echo $k['nama']?></td>
            <td><?php echo $k['umur']?></td>
            <td><?php echo $k['nilai']?></td>
            <td>
                <?php if ($k['nilai'] < 70) :?>
                    B
                <?php else :?>
                    A
                <?php endif;?>
            </td>
        </tr>
    <?php endforeach;?>
</table>

</body>
</html>