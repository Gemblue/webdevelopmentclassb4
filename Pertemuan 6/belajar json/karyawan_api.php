<table border="1">

<tr>
    <th>First Name</th>
    <th>Last Name</th>
    <th>Photo</th>
</tr>

<?php
$content = file_get_contents('https://reqres.in/api/users?page=1');
$users = json_decode($content, true);

foreach($users['data'] as $u)
{
    ?>
    <tr>
        <td><?php echo $u['first_name']?></td>
        <td><?php echo $u['last_name']?></td>
        <td><img width="100" src="<?php echo $u['avatar']?>" /></td>
    </tr>
    <?php
}
?>

</table>