<?php

$message = "Hello world!";

echo strlen($message) . "<br>";
echo str_word_count($message) . "<br>";

echo "<hr/>";

$teks = "Budi sekolah di SD Kertajaya";
echo strtoupper($teks) . "<br>";
echo strtolower($teks) . "<br>";
echo ucfirst($teks) . "<br>";
echo ucwords($teks) . "<br>";

echo "<hr/>";

$url = "http://codepolitan.com/interactive-coding/php/echo";
$segmen = explode("/", $url);
print_r($segmen);

echo '<hr/>';

$nama = "Budi/";

echo rtrim($nama, "/");

$nama = "/Sarah";

echo trim($nama, "/");