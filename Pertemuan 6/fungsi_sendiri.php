<?php

function bold($kalimat)
{
    return "<b>" . $kalimat . "</b>";
}

function hitungLingkaran($r)
{
    return 3.14 * $r * $r;
}

function hitungHarga($waktu, $tarif)
{
    return $waktu * $tarif;
}

echo hitungLingkaran(5);

echo bold("Ini contoh kalimat");